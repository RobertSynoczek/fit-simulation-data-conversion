#!/usr/bin/env python
# coding: utf-8

# import sys
# sys.modules[__name__].__dict__.clear()


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mplc
from typing import List
import copy


# FIXME: think about using some dedicated path module
# ------- DATA OBJECTS -------


class ChannelData:
    """
    Holds information for one channel (adc and time)
    """

    def __init__(self, adc, tdc, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.adc = adc
        self.tdc = tdc

    def __repr__(self):
        return "channel data: {} {}\n".format(self.adc, self.tdc)

    def is_above_threshold(self, threshold, only_timed_measurements=False) -> bool:
        """
        Checks if adc is above given threshold:

        :param threshold: the adc threshold
        :type threshold: int
        :param only_timed_measurements: if True, checks only for non-zero timed measurements
        :type only_timed_measurements: bool

        :return:
        :rtype: bool
        """
        if not only_timed_measurements:
            if self.adc >= threshold:
                return True
            return False
        else:
            if self.adc >= threshold and self.tdc != -1024.0:
                return True
            return False


class InputDataEntry:
    """
    Holds information for each event (channel adc and time, trigger information after calculation)
    """

    def __init__(self, data_index, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data_index: int = data_index
        self.channels = []
        self.triggers = {"MinBias": 0, "Multiplicity": 0, "Centrality": 0, "SemiCentrality": 0}

    def __repr__(self):
        return "{}\n {}\n {}".format(self.data_index, self.triggers, self.channels)

    def append(self, channel_data):
        """
        List.append wrapper for channel data

        :param channel_data: ChannelData object to be appended to InputDataEntry channel list
        :type channel_data: ChannelData

        :return: None
        :rtype: None
        """
        self.channels.append(channel_data)

    def is_empty(self) -> bool:
        """
        Checks if adc = 0 in all channels in entry

        :return: True, if entry is "empty"
        :rtype: bool
        """
        for i in range(48):
            if self.channels[i].adc != 0:
                return False
        return True

    def only_use_channels(self, channel_list: List[int]) -> None:
        """
        Deletes data in channels not listen in <channel_data>

        :param channel_list: list of active channels
        :type channel_list: List[int]

        :return: None
        :rtype: None
        """
        for index, channel in enumerate(self.channels):
            if index not in channel_list:
                channel.tdc = -1024.0
                channel.adc = 0

    def adc_sum(self, only_timed_measurements=False) -> int:
        """
        Sums up the adc from each channel

        :param only_timed_measurements: if True, checks only for non-zero timed measurements
        :type only_timed_measurements: bool

        :return: sum of all adc in entry
        :rtype: int
        """
        return_adc_sum = 0
        for i in range(48):
            if not only_timed_measurements:
                return_adc_sum += self.channels[i].adc
            else:
                if self.channels[i].tdc != -1024.0:
                    return_adc_sum += self.channels[i].adc

        return return_adc_sum

    def num_of_channels_above_threshold(self, threshold, only_timed_measurements=False) -> int:
        """
        Counts the channels above given threshold

        :param threshold: the adc threshold
        :type threshold: int
        :param only_timed_measurements: if True, checks only for non-zero timed measurements
        :type only_timed_measurements: bool

        :return: number of channels in entry above threshold
        :rtype: int
        """
        channel_count = 0
        for i in range(48):
            if self.channels[i].is_above_threshold(threshold, only_timed_measurements):
                channel_count += 1
        return channel_count

    def minimum_bias(self, b_threshold, only_timed_measurements=False) -> bool:
        """
        Checks minimum bias trigger (number of channels above adc threshold) for entry

        :param b_threshold: minimum bias threshold
        :type b_threshold: int
        :param only_timed_measurements: if True, checks only for non-zero timed measurements
        :type only_timed_measurements: bool

        :return: True, if at least one channels adc is above threshold
        :rtype: bool
        """
        if self.num_of_channels_above_threshold(b_threshold, only_timed_measurements) > 0:
            return True
        else:
            return False

    def multiplicity(self, mip, m_threshold, only_timed_measurements=False, discard_remainder=True) -> bool:
        """
        Checks multiplicity trigger (number of channel with mip counts above threshold) for entry

        :param mip: Minimum Ionizing Particle value
        :type mip: int
        :param m_threshold: multiplicity threshold (in minimum number of particles required in whole detector)
        :type m_threshold: int
        :param only_timed_measurements: if True, checks only for non-zero timed measurements
        :type only_timed_measurements: bool
        :param discard_remainder: If True, the remainder of division during the counting of mip's is omitted
        :type discard_remainder: bool

        :return: True, if number of particles in detector is above given threshold
        :rtype: bool
        """
        if discard_remainder:
            if self.adc_sum(only_timed_measurements) // mip >= m_threshold:
                return True
            return False
        else:
            if self.adc_sum(only_timed_measurements) / mip >= m_threshold:
                return True
            return False

    def centrality(self, c_threshold_low, c_threshold_high, only_timed_measurements=False) -> bool:
        """
        Checks centrality trigger (number of channel with adc within range <c_threshold_low; c_threshold_high>)
        for entry

        :param c_threshold_low: low centrality threshold (minimum adc value required)
        :type c_threshold_low: int
        :param c_threshold_high: high centrality threshold (maximum adc value required)
        :type c_threshold_high: int
        :param only_timed_measurements: if True, checks only for non-zero timed measurements
        :type only_timed_measurements: bool

        :return: True, if sum adc in detector is between low and high threshold
        :rtype: bool
        """
        if c_threshold_low <= self.adc_sum(only_timed_measurements) <= c_threshold_high:
            return True
        else:
            return False

    def semicentrality(self, s_threshold_low, s_threshold_high, only_timed_measurements=False) -> bool:
        """
        Checks semicentrality trigger (number of channel with adc within range <s_threshold_low; s_threshold_high>)
        for entry

        :param s_threshold_low: low semicentrality threshold (minimum adc value required)
        :type s_threshold_low: int
        :param s_threshold_high: high semicentrality threshold (maximum adc value required)
        :type s_threshold_high: int
        :param only_timed_measurements: if True, checks only for non-zero timed measurements
        :type only_timed_measurements: bool

        :return: True, if sum adc in detector is between low and high threshold
        :rtype: bool
        """
        if s_threshold_low <= self.adc_sum(only_timed_measurements) <= s_threshold_high:
            return True
        else:
            return False

    def check_triggers(self, b_threshold, m_threshold, mip, c_threshold_low, c_threshold_high,
                       s_threshold_low, s_threshold_high, only_timed_measurements=False,
                       discard_remainder=True) -> None:
        """
        Checks and saves trigger data for minimum bias, multiplicity and centrality triggers for entry

        :param b_threshold: minimum bias threshold
        :type b_threshold: int
        :param mip: Minimum Ionizing Particle value
        :type mip: int
        :param m_threshold: multiplicity threshold (in minimum number of particles required in whole detector)
        :type m_threshold: int
        :param c_threshold_low: low centrality threshold (minimum adc value required)
        :type c_threshold_low: int
        :param c_threshold_high: high centrality threshold (maximum adc value required)
        :type c_threshold_high: int
        :param s_threshold_low: low semicentrality threshold (minimum adc value required)
        :type s_threshold_low: int
        :param s_threshold_high: high semicentrality threshold (maximum adc value required)
        :type s_threshold_high: int
        :param only_timed_measurements: if True, checks only for non-zero timed measurements
        :type only_timed_measurements: bool
        :param discard_remainder: If True, the remainder of division during the counting of mip's is omitted
        :type discard_remainder: bool

        :return: None
        :rtype: None
        """
        self.triggers["MinBias"] = self.minimum_bias(b_threshold, only_timed_measurements)
        self.triggers["Multiplicity"] = self.multiplicity(m_threshold, mip, only_timed_measurements, discard_remainder)
        self.triggers["Centrality"] = self.centrality(c_threshold_low, c_threshold_high, only_timed_measurements)
        self.triggers["SemiCentrality"] = self.semicentrality(s_threshold_low, s_threshold_high,
                                                              only_timed_measurements)


# ----------- DATA LOADING AND SAVING ------------------


def load_simulation_data(name) -> List[InputDataEntry]:
    """
    Loads each entry from file <name> into list of InputDataEntry

    :param name: name of file, or path to file, to read the data from
    :type name: str

    :return: None
    :rtype: None
    """
    input_data = []
    with open(name) as trig_file:
        while True:
            trig_data_index = trig_file.readline()
            if not trig_data_index:
                break
            trig_data_index = int(trig_data_index.strip())
            next_entry = InputDataEntry(trig_data_index)
            for i in range(48):
                line_data = trig_file.readline()
                channel_index, adc_data, tdc_data = line_data.split()
                next_entry.append(ChannelData(int(adc_data), float(tdc_data)))

            input_data.append(next_entry)

    return input_data


def save_simulation_data(data, name: str) -> None:
    """
    Saves calculated simulation trigger data.
    \n
    Data format:\n
    <entry number (int)>    <minimum bias (bool)>  <multiplicity (bool)>  <centrality (bool)>\n
    <channel number (int)>    <adc (int)>   <tdc (float)>\n
    ...\n
    <channel number (int)>    <adc (int)>   <tdc (float)>\n
    \n
    Path to save file can be added by using file path as save file name. Please use Linux file path convention
    by using '/' (or '|bsh| |bsh|') instead of '|bsh|'.


    :param data: List of InputDataEntry objects to be saved
    :type data: List[InputDataEntry]
    :param name: Name of created file or path to file, including file extension
    :type name: str

    :return: None
    :rtype: None

    .. |bsh| unicode:: U+005C .. backslash sign
    .. |bck| unicode:: U+0008 .. backspace sign
    """
    # TODO: rewrite for possible bit format
    with open(name, "w") as new_file:
        for entry in data:
            new_file.write("{}\t{}\t{}\t{}\t{}\n".format(entry.data_index, int(entry.triggers['MinBias']),
                                                         int(entry.triggers['Multiplicity']),
                                                         int(entry.triggers['Centrality']),
                                                         int(entry.triggers["SemiCentrality"])))
            for index, channeldata in zip(enumerate(entry.channels), entry.channels):
                new_file.write("{}\t{}\t{}\n".format(index[0], channeldata.adc, channeldata.tdc))


# ------------- DATA MANIPULATION -----------------


def delete_zero_data(data):
    """
    Returns list of events, where at least one channel detected any event

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]

    :return: List of InputDataEntry objects without "empty" entries
    :rtype: List[InputDataEntry]
    """
    no_zero_data = [x for x in data if not x.is_empty()]
    return no_zero_data


def data_adc_threshold(data) -> int:
    """
    Calculates the adc threshold from mip data [for mip simulation data only].

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]

    :return: Calculated threshold (1/3 of average adc)
    :rtype: int
    """
    average = 0
    for i in data:
        average += i.adc_sum()
    average = average // len(data)
    return average


def data_adc_sum(data) -> int:
    """
    Sums adc in all channels of all entries in data

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]

    :return:
    :rtype: int
    """
    adc_sum_all = 0
    for i in data:
        adc_sum_all += i.adc_sum()
    return adc_sum_all


def only_use_data_in_channels(data, channel_list: List[int]):
    """
    Deletes data in channels that are not listed on <channel_list> for each entry of <data>, channels numbered from 0 to 47

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]
    :param channel_list: List of active channels numbered from 0 to 47
    :type channel_list: List[int]

    :return: List of InputDataEntry objects with only channels from channel_list active
    :rtype: List[InputDataEntry]
    """
    corrected_data = copy.deepcopy(data)

    for entry in corrected_data:
        entry.only_use_channels(channel_list)

    return corrected_data


def return_time_mean(data):
    """
    Returns per channel time mean of whole data set

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]

    :return: 48x1 matrix of time means from each channel in whole data set
    :rtype: np.ndarray[48,1]
    """
    dt_sum_vector = np.zeros([48, 1])
    dt_count_vector = np.zeros([48, 1])

    for entry in data:
        for index, channel_data in enumerate(entry.channels):
            if channel_data.tdc != -1024.0 and channel_data.adc != 0:
                dt_sum_vector[index] += channel_data.tdc
                dt_count_vector[index] += 1

    #     for i in dt_count_vector:
    #         print(i[0])

    dt_sum_vector = np.divide(dt_sum_vector, dt_count_vector, where=dt_count_vector != 0)
    return dt_sum_vector


def return_adc_mean(data):
    """
    Returns per channel adc mean of whole data set

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]

    :return: 48x1 matrix of adc means from each channel in whole data set
    :rtype: np.ndarray[48,1]
    """
    adc_sum_vector = np.zeros([48, 1])
    adc_count_vector = np.zeros([48, 1])

    for entry in data:
        for index, channel_data in enumerate(entry.channels):
            if channel_data.adc != 0 and channel_data.tdc != -1024.0:
                adc_sum_vector[index] += channel_data.adc
                adc_count_vector[index] += 1

    #     for i in adc_count_vector:
    #         print(i[0])

    adc_sum_vector = np.floor_divide(adc_sum_vector, adc_count_vector, where=adc_count_vector != 0)
    return adc_sum_vector


def return_adc_count_above_threshold(data, threshold):
    """
    Returns number of hits with adc above threshold per channel in data set

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]
    :param threshold: adc threshold
    :type threshold: int

    :return: 48x1 matrix of numbers of entries with adc above threshold
    :rtype: np.ndarray[48,1]
    """
    adc_count_vector = np.zeros([48, 1])

    for entry in data:
        for index, channel_data in enumerate(entry.channels):
            if channel_data.adc > threshold and channel_data.tdc != -1024.0:
                adc_count_vector[index] += 1

    #     for i in adc_count_vector:
    #         print(i[0])

    return adc_count_vector


def return_time_count_in_window(data, gate__time: float = 2.5):
    """
    Returns number of hits with time within window ( <-time; time>, time = 2.5 by deafult) per channel in data set

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]
    :param gate__time: the time from the middle of the window to one of its sides (window width = 2 x <gate_time>)
    :type gate__time: float

    :return:  48x1 matrix of numbers of entries with time within window
    :rtype: np.ndarray[48,1]
    """
    adc_count_vector = np.zeros([48, 1])

    for entry in data:
        for index, channel_data in enumerate(entry.channels):
            if channel_data.adc != 0 and abs(channel_data.tdc) <= gate__time:
                adc_count_vector[index] += 1

    #     for i in adc_count_vector:
    #         print(i[0])

    return adc_count_vector


def return_events_in_time_window(data, gate__time: float = 2.5):
    """
    Returns new data set, with events corrected, by deleting any channel data with time outside of given window
    ( <-time; time>, time = 2.5 by deafult) per channel in data set

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]
    :param gate__time: the time from the middle of the window to one of its sides (window width = 2 x <gate_time>)
    :type gate__time: float

    :return: data set, with events corrected, by deleting any channel data with time outside of given window
    :rtype: List[InputDataEntry]
    """
    corrected_data = copy.deepcopy(data)

    for entry in corrected_data:
        for index, channel_data in enumerate(entry.channels):
            if abs(channel_data.tdc) > gate__time:
                entry.channels[index].adc = 0
                entry.channels[index].tdc = -1024.0

    return corrected_data


def move_time_by(data, time_mean):
    """
    Moves time in each channel by given per channel time (time_mean is a [48,1] vector)

    :param data: List of InputDataEntry objects
    :type data: List[InputDataEntry]
    :param time_mean: 48x1 matrix of mean times in each channel from whole data set
    :type time_mean: np.ndarray[48,1]

    :return: data set with time in each channel of each entry moved by given <time_mean>
    :rtype: List[InputDataEntry]
    """
    data_with_offset = copy.deepcopy(data)
    for entry in data_with_offset:
        for idx, channel in enumerate(entry.channels):
            if channel.tdc != -1024.0:
                channel.tdc = np.round(channel.tdc - time_mean[idx][0], 4)

    return data_with_offset


# --------------- GRAPHICS AND REPRESENTATION --------------------

def draw_channel_data(color_map) -> None:
    """
    Generates graphical representation of detector with values from <color_map>, mapped accordingly to channel number.
    Use only for representational purposes.

    :param color_map: 48 element list of tuples representing "colors" of each channel in RGBA format
    :type color_map: List[Tuple[int,int,int,1]]

    :return: None
    :rtype: None
    """
    plt.polar()
    plt.xticks([0, np.pi / 4, np.pi / 2, 3 * np.pi / 4, np.pi, 5 * np.pi / 4, 3 * np.pi / 2, 7 * np.pi / 4, 2 * np.pi],
               [])
    plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2], [])
    for i, j in zip([1, 2, 3, 4, 5, 6], [0, 0.2, 0.4, 0.6, 0.8, 1.0]):
        plt.fill_between(np.linspace(np.pi / 2, 3 * np.pi / 4, 100), j, j + 0.2, color=color_map[8 * i - 4])
        plt.fill_between(np.linspace(3 * np.pi / 4, np.pi, 100), j, j + 0.2, color=color_map[8 * i - 3])
        plt.fill_between(np.linspace(-3 * np.pi / 4, -np.pi, 100), j, j + 0.2, color=color_map[8 * i - 2])
        plt.fill_between(np.linspace(-np.pi / 2, -3 * np.pi / 4, 100), j, j + 0.2, color=color_map[8 * i - 1])
        plt.fill_between(np.linspace(-np.pi / 4, -np.pi / 2, 100), j, j + 0.2, color=color_map[8 * i - 5])
        plt.fill_between(np.linspace(0, -np.pi / 4, 100), j, j + 0.2, color=color_map[8 * i - 6])
        plt.fill_between(np.linspace(0, np.pi / 4, 100), j, j + 0.2, color=color_map[8 * i - 7])
        plt.fill_between(np.linspace(np.pi / 4, np.pi / 2, 100), j, j + 0.2, color=color_map[8 * i - 8])


def draw_channel_data_for_set(data_set, title_str="Detector", save_name=None) -> None:
    """
    Shows graphical representation of detector with values from <data_set>, mapped accordingly to channel number.
    The values are mapped to a range of grey, where black is lowest  and white is highest. Use only for representational
    purposes. Data can be saved by adding named parameter <save_name>, with is the name of the saved file. Title of
    graph can be changed to <title_str>.

    :param data_set: 48x1 matrix of per channel data to be represented
    :type data_set: np.ndarray[48,1]
    :param title_str: title of created graph
    :type title_str: str
    :param save_name: name and extension of file with image of graph
    :type save_name: str

    :return: None
    :rtype: None
    """
    minimum = np.min(data_set[np.nonzero(data_set)])
    maximum = np.max(data_set[np.nonzero(data_set)])
    data_set[np.nonzero(data_set)] = (data_set[np.nonzero(data_set)] - np.min(data_set[np.nonzero(data_set)])) / (
            np.max(data_set[np.nonzero(data_set)]) - np.min(data_set[np.nonzero(data_set)]))
    # print(dead_time)
    colors = [(i[0], i[0], i[0], 1) for i in data_set]
    draw_channel_data(colors[0:48])
    plt.title(title_str)

    en = 21
    cmap = plt.get_cmap('gist_gray', en)

    norm = mplc.Normalize(vmin=minimum, vmax=maximum)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array(np.ndarray([]))
    plt.colorbar(sm, ticks=np.linspace(minimum, maximum, en),
                 boundaries=np.arange(minimum, maximum + (abs(minimum - maximum)) / en, (abs(minimum - maximum)) / en))

    plt.show()
    if save_name is not None:
        plt.savefig(save_name)


# ---------------- OUTPUT FILE GENERATION ---------------------
# TODO: bug-test the channel list option

def generate_output_file(input_file_name: str,
                         output_file_name: str,
                         b_threshold: int,
                         m_threshold: int,
                         mip: int,
                         c_threshold_low: int,
                         c_threshold_high: int,
                         gate_time: float = 2.5,
                         channel_list: List[int] = None,
                         reset_time_for_events_outside_of_window: bool = True,
                         remove_dead_time: bool = True,
                         delete_all_empty_events: bool = False,
                         only_timed_measurements: bool = False,
                         discard_remainder: bool = True):
    """
    Generates file with checked trigger options for given input file (additional option written in parameter list)

    :param input_file_name: name or path of file to convert
    :type: str
    :param output_file_name: name or path of file after conversion
    :type output_file_name: str
    :param b_threshold: Minimum bias threshold
    :type b_threshold: int
    :param m_threshold: Multiplicity threshold (check doc for InputDataEntry.multiplicity for more details)
    :type m_threshold: int
    :param mip: Value of Minimum Ionizing Particle for given detector
    :type mip: int
    :param c_threshold_low: low boundary for sum of adc in each channel
    :type c_threshold_low: int
    :param c_threshold_high: high boundary for sum of adc in each channel
    :type c_threshold_high: int
    :param gate_time: time of window in each direction symmetrically (default 2.5ns, so window size of 5 ns)
    :type gate_time: float
    :param channel_list: List of channels used in given test (default None, all channels are used)
    :type channel_list: List[int]
    :param reset_time_for_events_outside_of_window: if true, returns output only for channel data inside of given window (default true)
    :type reset_time_for_events_outside_of_window: bool
    :param remove_dead_time: if true, remove dead time in system, by subtracting mean time in each channel (default true)
    :type remove_dead_time: bool
    :param delete_all_empty_events: if true, deletes all events in with no data was recorded (default false)
    :type delete_all_empty_events: bool
    :param only_timed_measurements: if true, all events witch recorded an adc value but not an time value are deleted (default false)
    :type only_timed_measurements: bool
    :param discard_remainder: if true, the reminder of division in checking multiplicity rigger is discarded (default true)
    :type discard_remainder: bool

    :return:
    :rtype: None
    """
    data = load_simulation_data(input_file_name)

    if channel_list is not None:
        data = only_use_data_in_channels(data, channel_list)

    if delete_all_empty_events:
        data = delete_zero_data(data)

    if remove_dead_time:
        dead_time = return_time_mean(data)
        data = move_time_by(data, dead_time)

    if reset_time_for_events_outside_of_window:
        data = return_events_in_time_window(data, gate_time)

    for entry in data:
        entry.check_triggers(b_threshold, m_threshold, mip, c_threshold_low, c_threshold_high, only_timed_measurements,
                             discard_remainder)
    save_simulation_data(data, output_file_name)

# if __name__ == "__main__":
#     generate_output_file("FV0_digit.dat",
#                          'C:/Users/becik/Desktop/sorting_skeleton/fit-simulation-data-conversion/test_save.dat',
#                          8, 10, 27, 100, 1000, delete_all_empty_events=True, channel_list=[1, 2, 3, 4])
